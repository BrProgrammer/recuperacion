package facci.pm.avila.alvaro.recuperacion.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import facci.pm.avila.alvaro.recuperacion.Actividades.DetalleActivity;
import facci.pm.avila.alvaro.recuperacion.Modelo.Estudiantes;
import facci.pm.avila.alvaro.recuperacion.R;

public class AdapterEstudiantes extends RecyclerView.Adapter<AdapterEstudiantes.ViewHolder>{

    private ArrayList<Estudiantes> estudiantesArrayList;

    public AdapterEstudiantes(ArrayList<Estudiantes> estudiantesArrayList) {
        this.estudiantesArrayList = estudiantesArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_estudiantes, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Estudiantes estudiantes = estudiantesArrayList.get(position);
        holder.nombres.setText(estudiantes.getNombres());
        holder.apellidos.setText(estudiantes.getApellidos());
        holder.nota_uno.setText(estudiantes.getNota_uno());
        holder.nota_dos.setText(estudiantes.getNota_dos());
        Picasso.get().load(estudiantes.getImagen()).into(holder.imagenEstudiante);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DetalleActivity.class);
                intent.putExtra("id", estudiantes.getId());
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return estudiantesArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView imagenEstudiante;
        private TextView nombres, apellidos, nota_uno, nota_dos;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imagenEstudiante = (ImageView)itemView.findViewById(R.id.ImagenEstudiantes);
            nombres = (TextView)itemView.findViewById(R.id.LBLNombres);
            apellidos = (TextView)itemView.findViewById(R.id.LBLApellidos);
            nota_uno = (TextView)itemView.findViewById(R.id.LBLParcialUno);
            nota_dos = (TextView)itemView.findViewById(R.id.LBLParcialDos);
        }
    }
}
