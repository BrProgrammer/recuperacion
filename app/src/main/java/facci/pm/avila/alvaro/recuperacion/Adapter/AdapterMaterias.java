package facci.pm.avila.alvaro.recuperacion.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import facci.pm.avila.alvaro.recuperacion.Modelo.Materias;
import facci.pm.avila.alvaro.recuperacion.R;

public class AdapterMaterias extends RecyclerView.Adapter<AdapterMaterias.ViewHolder>{

    private ArrayList<Materias> materiasArrayList;

    public AdapterMaterias(ArrayList<Materias> materiasArrayList) {
        this.materiasArrayList = materiasArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_materias, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Materias materias = materiasArrayList.get(position);
            holder.descripcion.setText(materias.getDescripcion());
            holder.notaDos.setText(materias.getNotad());
            holder.notaUno.setText(materias.getNotau());
            Picasso.get().load(materias.getImagen()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return materiasArrayList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView descripcion, notaUno, notaDos;
        private ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            descripcion = (TextView)itemView.findViewById(R.id.LBLDescripcion);
            notaUno = (TextView)itemView.findViewById(R.id.LBLParcialUnoM);
            notaDos = (TextView)itemView.findViewById(R.id.LBLParcialDosM);
            imageView = (ImageView)itemView.findViewById(R.id.ImagenM);
        }
    }
}
