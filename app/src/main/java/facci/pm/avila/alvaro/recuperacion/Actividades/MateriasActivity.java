package facci.pm.avila.alvaro.recuperacion.Actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import facci.pm.avila.alvaro.recuperacion.Adapter.AdapterEstudiantes;
import facci.pm.avila.alvaro.recuperacion.Adapter.AdapterMaterias;
import facci.pm.avila.alvaro.recuperacion.Modelo.Estudiantes;
import facci.pm.avila.alvaro.recuperacion.Modelo.Materias;
import facci.pm.avila.alvaro.recuperacion.R;

public class MateriasActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Materias> materiasArrayList;
    private AdapterMaterias adapterMaterias;
    private static final String URLMaterias = "http://10.32.7.211:3005/api/materias/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materias);

        recyclerView = (RecyclerView)findViewById(R.id.RecyclerMaterias);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        materiasArrayList = new ArrayList<>();
        adapterMaterias = new AdapterMaterias(materiasArrayList);

        String id = getIntent().getStringExtra("id");

        MateriasLista(id);
    }

    private void MateriasLista(String id) {

            StringRequest stringRequest = new StringRequest(Request.Method.GET, URLMaterias+id, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        Log.e("gggg", response);
                        for (int i = 0; i<jsonArray.length(); i++){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            Materias materias = new Materias();
                            materias.setDescripcion(jsonObject.getString("descripcion"));
                            materias.setImagen(jsonObject.getString("imagen"));
                            materias.setNotad(jsonObject.getString("parcial_dos"));
                            materias.setNotau(jsonObject.getString("parcial_uno"));
                            materiasArrayList.add(materias);
                        }
                        recyclerView.setAdapter(adapterMaterias);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Error: ", error.toString());
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
    }
}
