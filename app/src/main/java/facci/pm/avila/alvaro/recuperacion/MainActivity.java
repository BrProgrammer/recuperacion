package facci.pm.avila.alvaro.recuperacion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import facci.pm.avila.alvaro.recuperacion.Adapter.AdapterEstudiantes;
import facci.pm.avila.alvaro.recuperacion.Modelo.Estudiantes;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Estudiantes> estudiantesArrayList;
    private AdapterEstudiantes adapterEstudiantes;
    private static final String URLEstudiante = "http://10.32.7.211:3005/api/estudiantes/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView)findViewById(R.id.RecyclerUno);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        estudiantesArrayList = new ArrayList<>();
        adapterEstudiantes = new AdapterEstudiantes(estudiantesArrayList);

        EstudiantesL();
    }

    private void EstudiantesL() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLEstudiante, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Estudiantes estudiantes = new Estudiantes();
                        estudiantes.setId(jsonObject.getString("id"));
                        estudiantes.setNombres(jsonObject.getString("nombres"));
                        estudiantes.setApellidos(jsonObject.getString("apellidos"));
                        estudiantes.setImagen(jsonObject.getString("imagen"));
                        estudiantes.setNota_dos(jsonObject.getString("parcial_dos"));
                        estudiantes.setNota_uno(jsonObject.getString("parcial_uno"));
                        estudiantesArrayList.add(estudiantes);
                    }
                    recyclerView.setAdapter(adapterEstudiantes);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error: ", error.toString());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
