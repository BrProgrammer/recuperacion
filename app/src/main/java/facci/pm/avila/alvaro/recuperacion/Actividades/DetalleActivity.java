package facci.pm.avila.alvaro.recuperacion.Actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import facci.pm.avila.alvaro.recuperacion.R;

public class DetalleActivity extends AppCompatActivity {

    private TextView nombres, apellidos, notaU, notaD;
    private ImageView imageView;
    private static final String URLEstudiantes = "http://10.32.7.211:3005/api/estudiantes/";
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        final String id = getIntent().getStringExtra("id");

        nombres = (TextView)findViewById(R.id.NombresD);
        apellidos = (TextView)findViewById(R.id.ApellidosD);
        notaU = (TextView)findViewById(R.id.NotaUno);
        notaD = (TextView)findViewById(R.id.NotaDos);
        imageView = (ImageView)findViewById(R.id.Img);
        button = (Button)findViewById(R.id.Btn);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetalleActivity.this, MateriasActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLEstudiantes + id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    nombres.setText(jsonObject.getString("nombres"));
                    apellidos.setText(jsonObject.getString("apellidos"));
                    notaU.setText(jsonObject.getString("parcial_uno"));
                    notaD.setText(jsonObject.getString("parcial_dos"));
                    Picasso.get().load(jsonObject.getString("imagen")).into(imageView);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}
