package facci.pm.avila.alvaro.recuperacion.Modelo;

public class Estudiantes {

    private String id, nombres, apellidos, nota_uno, nota_dos, imagen;

    public Estudiantes() {
    }

    public Estudiantes(String id, String nombres, String apellidos, String nota_uno, String nota_dos, String imagen) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.nota_uno = nota_uno;
        this.nota_dos = nota_dos;
        this.imagen = imagen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNota_uno() {
        return nota_uno;
    }

    public void setNota_uno(String nota_uno) {
        this.nota_uno = nota_uno;
    }

    public String getNota_dos() {
        return nota_dos;
    }

    public void setNota_dos(String nota_dos) {
        this.nota_dos = nota_dos;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
