package facci.pm.avila.alvaro.recuperacion.Modelo;

public class Materias {

    private String descripcion, notau, notad, imagen;

    public Materias() {
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNotau() {
        return notau;
    }

    public void setNotau(String notau) {
        this.notau = notau;
    }

    public String getNotad() {
        return notad;
    }

    public void setNotad(String notad) {
        this.notad = notad;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
